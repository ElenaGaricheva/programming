#include <stdio.h>

int main() {
	char str[256], p;
	int i = 0, j = 0, k = 0;
	printf("Enter a string: ");
	scanf("%s", &str);
	while (i<256) {
		if (str[i - 1] && str[i]>0) {
			j = (str[i] >= 'a' && str[i] <= 'z') ? str[i] - 128 : str[i];
			k = (str[i-1] >= 'a' && str[i-1] <= 'z') ? str[i-1] - 128 : str[i-1];
			if (j < k) {
				p = str[i];
				str[i] = str[i - 1];
				str[i - 1] = p;
				i -= 2;
			}
		}
		i++;
	};
	printf("%s\n", str);
	return 0;
}