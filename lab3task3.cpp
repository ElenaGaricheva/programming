#include <stdio.h>
#include <stdlib.h>


int main () 
{
    char buf[]={"All are swimming, and I am writing the programs."};
    char * firstSymb[50];
    char * lastSymb[50];
    int i = 0,f = 0, inword=0;

    while (buf[i]) 
	{
        if (buf[i]!=' ' && inword==0) 
		{
            firstSymb[f]=&buf[i];
            inword=1;
        }
        else if (buf[i]==' ' && inword==1) 
		{
            inword=0;
            lastSymb[f]=&buf[i-1];
            f++;
        }
        i++;
    }
    lastSymb[f]=&buf[i-1];
    for (f; f>=0; f--) 
	{
            while (firstSymb[f]<=lastSymb[f]) 
			{
                    putchar(*firstSymb[f]++);
                }
        printf(" ");
    }
    return 0;
}