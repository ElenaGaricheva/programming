#include <stdio.h> 

int count=0, number=0;

int convert (char * source) {
    if (source[count]!='\n') {
        number=number*10 + source[count]-'0';
        count++;
        convert(source);
    }
    return number;
}

int main () {
    char source[80];
    printf("Enter a number: ");
    fgets(source,80,stdin);
    printf("%d \n",convert(source));
    return 0;
}